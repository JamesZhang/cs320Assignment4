#include <stdio.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

int main (int argc, char *argv[]) {
  printf("Assignment #4-1, James Zhang, zhangjames389@gmail.com \n");
  char buff[256];
  int error;
  lua_State *L = luaL_newstate();  /* opens Lua */
  luaL_openlibs(L);

  if (luaL_dofile(L, argv[1])) {
    fprintf(stderr, "%s", lua_tostring(L, -1));
  }

  lua_close(L);
  return 0;
}
