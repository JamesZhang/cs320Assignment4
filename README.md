James Zhang
zhangjames389@gmail.com

The fourth homework assignment for CS320 Fall2016.
This assignment is designed to get the user accustomed Lua and c working in multilanguage projets

The user shall weed out applicants applying to their firm. The applicants will supply a lua program that attempts to solve the Fizz Buzz test.
The user will make a basic lua environment and shell script to test their solutions and see if they have programming "chops".



**** All bash files must be made executable before running *****
**** To make executable type into terminal: chmod +x "insert bash file name" ****



prog4_1.c:
A basic interpreter the makes a basic environment in which we can run our lua files.

Compiled by : gcc prog4_1.c -llua -lm -ldl -L lua-5.3.3/src -I lua-5.3.4/src

prog4_2.lua: 
A lua solution to the Fizz Buzz test. For multiples of three it prins Fizz and multiples of five Buzz. Numbers which are multiples of both FizzBuzz is printed.
Does so for numbers 1-100

Ran as : ./a.out prog4_2.lua

*Requires the interpreter to be compiled first

prog4_3.sh:
Automates the assignment by compiling the earlier C code and takes in 2 arguments. First one being the lua solution to Fizz Buzz and the second being a file of the correct solution to the problem.
If the first argument matches the correct argument then "Passed Test" is output otherwise "Failed Test".

Ran as : ./prog4_3.sh prog4_2.lua correct.output
