#!/bin/bash

echo Assignment \#3-3, James Zhang, zhangjames389@gmail.com

LUA_FILE=$1
SOLUTION=$2
LUA_DIR=lua-5.3.3/src

# compile prog4_1.c to a.out executable
gcc prog4_1.c -llua -lm -ldl -L ${LUA_DIR} -I ${LUA_DIR}

./a.out ${LUA_FILE}|sed '1,2d' > test.output

if cmp -s "test.output" "${SOLUTION}"
then
   echo "Passed Test"
else
   echo "Failed Test"
fi
